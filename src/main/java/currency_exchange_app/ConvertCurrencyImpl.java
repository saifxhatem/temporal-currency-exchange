package currency_exchange_app;



public class ConvertCurrencyImpl implements ConvertCurrency {

    @Override
    public Double convertCurrency(Double conversionRate, Double number) {
        System.out.println("in convertCurrency(), conversionRate = " + conversionRate + " & number = " + number);
        return conversionRate * number;
    }
}