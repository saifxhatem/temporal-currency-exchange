package currency_exchange_app;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.temporal.activity.Activity;
import io.temporal.activity.ActivityExecutionContext;
import io.temporal.client.ActivityCompletionClient;
import io.vertx.core.Vertx;
import io.vertx.ext.web.client.WebClient;



import java.net.MalformedURLException;
import java.net.URL;

public class CallCurrencyApiImpl implements CallCurrencyApi {

    private final ActivityCompletionClient completionClient;

    public CallCurrencyApiImpl(ActivityCompletionClient completionClient) {
        this.completionClient = completionClient;
    }

    public String callCurrencyApi(String convertFrom, String convertTo) {

        ActivityExecutionContext ctx = Activity.getExecutionContext();
        byte[] taskToken = ctx.getInfo().getTaskToken();
        String API_KEY = "35fd9a9b7213d0b552ca";
        String EXCHANGE_STRING = convertFrom + "_" + convertTo;
        String urlString = "https://free.currconv.com/api/v7/convert?q=" + EXCHANGE_STRING + "&compact=ultra&apiKey=" + API_KEY;
        URL url = null;
        try {
            url = new URL(urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//        System.out.println(url);
        Vertx vertx = Vertx.vertx();
        WebClient client = WebClient.create(vertx);
        client
                .get(url.getDefaultPort(), url.getHost(), url.getFile())
                .ssl(true)
                .send()
                .onSuccess(response -> {
                        ObjectMapper mapper = new ObjectMapper();
                    try {
                        String jsonResponse = response.body().toString();
                        JsonNode jsonNode = mapper.readTree(jsonResponse);
                        String conversionRate = jsonNode.get(EXCHANGE_STRING).asText();
                        System.out.println("Conversion Rate: " + conversionRate);
                        completionClient.complete(taskToken, conversionRate);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }

                })
                .onFailure(err ->
                        System.out.println("Something went wrong " + err.getMessage()));
        ctx.doNotCompleteOnReturn();
        return "ignored";
    }
}