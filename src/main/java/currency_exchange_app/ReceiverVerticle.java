package currency_exchange_app;

import io.temporal.api.common.v1.WorkflowExecution;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;

public class ReceiverVerticle extends AbstractVerticle {
    @Override
    public void start() {
        System.out.println("Receiver verticle started.");
        vertx.eventBus().consumer("address", message -> {
            JsonObject request = (JsonObject) message.body();
//            System.out.println(request);
            String convertFrom = request.getString("convertFrom");
            String convertTo = request.getString("convertTo");
            Double amount = Double.parseDouble(request.getString("amount"));
            System.out.println("Signal received, starting workflow");
            WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
            WorkflowClient client = WorkflowClient.newInstance(service);
            WorkflowOptions options = WorkflowOptions.newBuilder()
                    .setTaskQueue(Shared.CURRENCY_EXCHANGE_APP_TASK_QUEUE)
                    .build();
            CurrencyExchangeWorkflow workflow = client.newWorkflowStub(CurrencyExchangeWorkflow.class, options);
            // ASYNC
//            WorkflowExecution we = WorkflowClient.start(workflow::conversionNumber, amount, convertFrom, convertTo);

            //SYNC
            String result = workflow.conversionNumber(amount, convertFrom, convertTo);
            message.reply(result);
        });
    }
}
