package currency_exchange_app;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface ConvertCurrency {

    @ActivityMethod
    Double convertCurrency(Double conversionRate, Double number);
}