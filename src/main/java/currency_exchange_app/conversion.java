package currency_exchange_app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class conversion {
    @JsonAlias("EGP_USD")
    @JsonProperty("EGP_USD")
    public Double EGP_USD;

    @JsonAlias("USD_EGP")
    @JsonProperty("USD_EGP")
    public Double USD_EGP;
}
