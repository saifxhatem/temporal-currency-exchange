package currency_exchange_app.app;

import currency_exchange_app.MainVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;


public class RunApp {
    public static void main(String[] args)
    {
        Vertx vertx = Vertx.vertx(new VertxOptions());
        // Deploy main Verticle
        vertx.deployVerticle(new MainVerticle());
    }
}