package currency_exchange_app;

import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;

public class InitiateCurrencyExchangeApp {

    public static void main(String[] args) throws Exception {
        // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        // WorkflowClient can be used to start, signal, query, cancel, and terminate Workflows.
        WorkflowClient client = WorkflowClient.newInstance(service);
        WorkflowOptions options = WorkflowOptions.newBuilder()
                .setTaskQueue(Shared.CURRENCY_EXCHANGE_APP_TASK_QUEUE)
                .build();
        // WorkflowStubs enable calls to methods as if the Workflow object is local, but actually perform an RPC.
        CurrencyExchangeWorkflow workflow = client.newWorkflowStub(CurrencyExchangeWorkflow.class, options);
        // Synchronously execute the Workflow and wait for the response.
        String result = workflow.conversionNumber(10.0, "AED", "EGP");
        System.out.println("in InitiateApp: Result = " + result);
        System.exit(0);
    }
}