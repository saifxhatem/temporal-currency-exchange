package currency_exchange_app;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface CallCurrencyApi {

    @ActivityMethod
    String callCurrencyApi(String convertFrom, String convertTo);
}