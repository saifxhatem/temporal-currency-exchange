package currency_exchange_app;

import io.temporal.api.common.v1.WorkflowExecution;
import io.temporal.client.WorkflowClient;
import io.temporal.client.WorkflowOptions;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;

public class MainVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {

        // Create a Router
        Router router = Router.router(vertx);
        Route route = router.route().path("/convert");

        // Mount the handler for all incoming requests at every path and HTTP method
        route.handler(context -> {
            // Get the address of the request
            String address = context.request().connection().remoteAddress().toString();
            // Get the query parameter "name"
            MultiMap queryParams = context.queryParams();
            String convertFrom = queryParams.contains("convertFrom") ? queryParams.get("convertFrom") : "USD";
            String convertTo = queryParams.contains("convertTo") ? queryParams.get("convertTo") : "EGP";
            String amount = queryParams.contains("amount") ? queryParams.get("amount") : "1000";
            JsonObject request = new JsonObject();
            request.put("convertFrom", convertFrom).put("convertTo", convertTo).put("amount", amount);


            vertx.deployVerticle(new ReceiverVerticle()).onSuccess(res -> {
                System.out.println("Server received HTTP request, now sending signal over");
                vertx.eventBus().request("address", request, ar -> {
                    if (ar.succeeded()) {
//                        System.out.println("Received reply: " + ar.result().body());
                        System.out.println("Reply received!");

                        context.json(
                                new JsonObject()
                                        .put("Success", "true")
                                        .put("conversion", ar.result().body())
                        );
                    }
                });
            });

            // Write a json response

        });

        // Create the HTTP server
        vertx.createHttpServer()
                // Handle every request using the router
                .requestHandler(router)
                // Start listening
                .listen(8888)
                // Print the port
                .onSuccess(server ->
                        System.out.println(
                                "HTTP server started on port " + server.actualPort()
                        )
                );
    }
}