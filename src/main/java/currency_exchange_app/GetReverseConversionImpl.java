package currency_exchange_app;



public class GetReverseConversionImpl implements GetReverseConversion {

    @Override
    public Double getReverseConversion(Double conversionRate, Double number) {
        return conversionRate * number;
    }

}