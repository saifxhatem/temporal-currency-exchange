package currency_exchange_app;

import io.temporal.activity.ActivityInterface;
import io.temporal.activity.ActivityMethod;

@ActivityInterface
public interface GetReverseConversion {

    @ActivityMethod
    Double getReverseConversion(Double conversionRate, Double number);
}