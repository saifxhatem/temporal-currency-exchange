package currency_exchange_app;

import io.temporal.activity.ActivityOptions;
import io.temporal.workflow.Workflow;
import java.time.Duration;


public class CurrencyExchangeWorkflowImpl implements CurrencyExchangeWorkflow {

    ActivityOptions options = ActivityOptions.newBuilder()
            .setScheduleToCloseTimeout(Duration.ofSeconds(5))
            .build();

    // ActivityStubs enable calls to Activities as if they are local methods, but actually perform an RPC.
    private final CallCurrencyApi apiConverter = Workflow.newActivityStub(CallCurrencyApi.class, options);
    private final ConvertCurrency convertCurrency = Workflow.newActivityStub(ConvertCurrency.class, options);
    private final GetReverseConversion getReverseConversion = Workflow.newActivityStub(GetReverseConversion.class, options);

    @Override
    public String conversionNumber(Double number, String ConvertFrom, String ConvertTo) {
        // This is the entry point to the Workflow.
        // If there were other Activity methods they would be orchestrated here or from within other Activities.
        System.out.println("In ConversionNumber(), Number = " + number + " convertFrom = " + ConvertFrom + " , convertTo = " + ConvertTo);
        String apiVal = apiConverter.callCurrencyApi(ConvertFrom, ConvertTo);
        Double conversionRate = Double.parseDouble(apiVal);
//        conversion reverseConversionRate = apiConverter.callCurrencyApi("EGP", "USD");
        Double convertedNumber = convertCurrency.convertCurrency(conversionRate, number);
//        Double reverseConvertedNumber = getReverseConversion.getReverseConversion(reverseConversionRate.EGP_USD, number);
//        String resultString = "Input Number: " + number + " / USD to EGP: " + convertedNumber + " / Reverse Conversion: " + reverseConvertedNumber;
        String resultString = convertedNumber.toString();
        System.out.println("In ConversionNumber: convertedNumber = " + convertedNumber);
        System.out.println("In ConversionNumber(): resultString = " + resultString);
        return resultString;

    }
}

