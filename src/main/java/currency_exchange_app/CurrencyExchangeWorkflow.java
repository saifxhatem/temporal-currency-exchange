package currency_exchange_app;

import io.temporal.workflow.WorkflowInterface;
import io.temporal.workflow.WorkflowMethod;

@WorkflowInterface
public interface CurrencyExchangeWorkflow {

    @WorkflowMethod
    String conversionNumber(Double number, String ConvertFrom, String ConvertTo);
}