package currency_exchange_app;

import io.temporal.client.ActivityCompletionClient;
import io.temporal.client.WorkflowClient;
import io.temporal.serviceclient.WorkflowServiceStubs;
import io.temporal.worker.Worker;
import io.temporal.worker.WorkerFactory;

public class CurrencyExchangeAppWorker {

    public static void main(String[] args) {
        // This gRPC stubs wrapper talks to the local docker instance of the Temporal service.
        WorkflowServiceStubs service = WorkflowServiceStubs.newInstance();
        WorkflowClient client = WorkflowClient.newInstance(service);
        // Create a Worker factory that can be used to create Workers that poll specific Task Queues.
        WorkerFactory factory = WorkerFactory.newInstance(client);
        Worker worker = factory.newWorker(Shared.CURRENCY_EXCHANGE_APP_TASK_QUEUE);
        // This Worker hosts both Workflow and Activity implementations.
        // Workflows are stateful, so you need to supply a type to create instances.
        worker.registerWorkflowImplementationTypes(CurrencyExchangeWorkflowImpl.class);
        // Activities are stateless and thread safe, so a shared instance is used.
//        worker.registerActivitiesImplementations(new FormatImpl());
        ActivityCompletionClient completionClient = client.newActivityCompletionClient();
        worker.registerActivitiesImplementations(
                new CallCurrencyApiImpl(completionClient),
                new ConvertCurrencyImpl(),
                new GetReverseConversionImpl()); //pass each implementation, only use cC in async
        // Start polling the Task Queue.
        factory.start();

//        CurrencyExchangeWorkflow workflow =
//                client.newWorkflowStub(
//                        CurrencyExchangeWorkflow.class,
//                        WorkflowOptions.newBuilder()
//                                .setTaskQueue(Shared.CURRENCY_EXCHANGE_APP_TASK_QUEUE)
//                                .build());
//
//        CompletableFuture<String> Result = WorkflowClient.execute(workflow::conversionNumber, 10.0);
//
//        // Wait for workflow execution to complete and display its results.
//        System.out.println(Result);
//        System.exit(0);
    }
}